import 'package:flutter/material.dart';
import 'LoginPage.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext contex){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RAC',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(), //LLamar el Widget del login
    );
  }
}

