import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      _LoginPageState(); //create State de LoginPage
}

enum FormType { login, register } // lo usamos para cambiar de login a register

class _LoginPageState extends State<LoginPage> {
  //Aqui sucede todo
  final formKey =
      GlobalKey<FormState>(); // se usa para acceder al FORM desde afuera

  String _email; //correo
  String _password; //contraseña
  FormType _formType = FormType.login; // por defecto el estado inicia en login

  ///////////////////////////////////////////////////////////////////////

  bool validateAndSave() {
    //funcion de validacion de estado para el logueo
    final form = formKey.currentState;
    if (form.validate()) {
      form.save(); //se guardan los datos una vez se verifican
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    //funcion asincrona de validacion
    if (validateAndSave()) {
      try {
        if (_formType == FormType.login) {
          FirebaseUser user =
              await FirebaseAuth.instance.signInWithEmailAndPassword(
                  //retorna un usuario
                  email: _email,
                  password: _password);
          print('Signed in: ${user.uid}'); // uid: user id
        } else {
          FirebaseUser user = await FirebaseAuth.instance
              .createUserWithEmailAndPassword(
                  email: _email, password: _password);
          print('Registered user: ${user.uid}');
        }
      } catch (e) {
        print('Error: $e ');
      }
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() { //setState hace rebuild de la app y se mira en que estado quedo.
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    setState(() {
      _formType = FormType.login;
    });
  }

/////////////////////////////////////////////////////////////////////////
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //scafold
      appBar: AppBar(
        //appbar
        centerTitle: true,
        title: Text("Welcome to RAC"),
        backgroundColor: const Color(0x4d6db6).withOpacity(0.5),
      ),
      body: Container(
        //container
        padding: EdgeInsets.all(40.0),
        child: Form(
          ///responsable de las validaciones
          key: formKey,
          child: Column(
            //column
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: buildInputs() + buildSubmitsButtons(), // se refactorizo
          ),
        ),
      ),
    );
  }

  List<Widget> buildInputs() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        validator: (value) =>
            value.isEmpty ? 'Email can\'t be empty' : null, //validator
        onSaved: (value) =>
            _email = value, // se guarda el dato en su respectiva variable
      ),
      TextFormField(
        decoration: InputDecoration(
          labelText: 'Password',
        ),
        validator: (value) =>
            value.isEmpty ? 'Password can\'t be empty' : null, //validator
        onSaved: (value) => _password = value,
        obscureText: true,
      ),
    ];
  }

  List<Widget> buildSubmitsButtons() {
    if (_formType == FormType.login) {
      ///login Switch
      return [
        RaisedButton(
          //button login
          child: Text(
            'Login',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed:
              validateAndSubmit, //se asume es un metodo por ende no se usa ()
        ),
        FlatButton(
          child: Text(
            'Create Account',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed: moveToRegister,
        ),
      ];
    } else {
      return [
        RaisedButton(
          //button create account
          child: Text(
            'Create Account',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed:
              validateAndSubmit, //se asume es un metodo por ende no se usa ()
        ),
        FlatButton(
          child: Text(
            'Have an account? Login',
            style: TextStyle(fontSize: 15.0),
          ),
          onPressed: moveToLogin,
        ),
      ];
    }
  }
}
